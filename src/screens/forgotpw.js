import React from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Logo from '../../assets/logo1.png';

const ForgotScreen = () => {
  return (
    <View style={styles.root}>
      <Image source={Logo} style={styles.logo} />
      <Text style={[styles.text, { marginBottom: 18 }]}>Reset your password</Text>
      <TextInput style={styles.input} placeholder="Email" />
      <TouchableOpacity style={styles.button} onPress={handleSignUp}>
        <Text style={styles.buttonText}>Request Reset</Text>
      </TouchableOpacity>
    </View>
  );
};

const handleSignUp = () => {
  alert('Sign up');
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 140,
    backgroundColor: '#F4F4F4',
  },
  text: {
    marginTop: 40,
    fontSize: 18,
  },
  input: {
    width: 273,
    height: 43,
    borderRadius: 10,
    backgroundColor: 'white',
    paddingLeft: 17,
    marginBottom: 17,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 12,
  },
  footer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 420,
    height: 55,
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 12,
    color:'#5B5B5B',
  },
  loginText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#5B5B5B',
    marginLeft: 5,
  },
  shadow:{
    shadowColor: 'black',
    shadowOffset: {width: 0, height: -4},
    shadowOpacity: 10,
    shadowRadius: 6,
    elevation: 50,
  },
  logo: {},
});

export default ForgotScreen;
