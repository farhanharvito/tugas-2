import React from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Logo from '../../assets/logo1.png';
import { NavigationContainer } from '@react-navigation/native';

const RegisterScreen = ({navigation}) => {
  return (
    <View style={styles.root}>
      <Image source={Logo} style={styles.logo} />
      <Text style={[styles.text, { marginBottom: 33 }]}>Create an account</Text>
      <TextInput style={styles.input} placeholder="Name" />
      <TextInput style={styles.input} placeholder="Email" />
      <TextInput style={styles.input} placeholder="Phone" keyboardType="numeric" />
      <TextInput style={styles.input} placeholder="Password" secureTextEntry={true} />
      <TouchableOpacity style={styles.button} onPress={handleSignUp}>
        <Text style={styles.buttonText}>Sign Up</Text>
      </TouchableOpacity>
      <View style={[styles.footer, styles.shadow]}>
        <Text style={styles.footerText}>Already have an account?</Text>
        <Text style={styles.loginText} onPress={()=>navigation.navigate('LoginScreen')}>Log in</Text>
      </View>
    </View>
  );
};

const handleSignUp = () => {
  alert('Sign up');
};

const styles = StyleSheet.create({
  root: {
    flex: 0,
    alignItems: 'center',
    paddingTop: 140,
    backgroundColor: '#F4F4F4',
  },
  text: {
    marginTop: 17,
    fontSize: 18,
  },
  input: {
    width: 273,
    height: 43,
    borderRadius: 10,
    backgroundColor: 'white',
    paddingLeft: 17,
    marginBottom: 17,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
  footer: {
    marginTop: 62,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 420,
    height: 55,
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 12,
    color:'#5B5B5B',
  },
  loginText: {
    fontSize: 12,
    color: '#5B5B5B',
    marginLeft: 5,
  },
  shadow:{
    shadowColor: 'black',
    shadowOffset: {width: 0, height: -4},
    shadowOpacity: 10,
    shadowRadius: 6,
    elevation: 50,
  },
  logo: {},
});

export default RegisterScreen;
