import React from 'react';
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Logo from '../../assets/logo1.png';
import FacebookLogo from '../../assets/facebook.png';
import EyeIcon from '../../assets/eye.png';
import GoogleLogo from '../../assets/google.png'
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';

const LoginScreen = ({navigation}) => {
  return (
    <View style={styles.root}>
      <Image source={Logo} style={styles.logo} />
      <Text style={[styles.text, { marginBottom: 33 }]}>Please Sign in to continue</Text>
      <TextInput style={styles.input} placeholder="Username" />
      <View style={styles.inputContainer}>
        <TextInput style={{flex: 1, fontSize: 12}} placeholder="Password" secureTextEntry={true}/>
        <Image source={EyeIcon}/>
      </View>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('BottomNav')}>
        <Text style={styles.buttonText}>Sign in</Text>
      </TouchableOpacity>
      <Text style={{color: '#2E3283', fontSize: 12, marginTop: 17}} onPress={() => navigation.navigate('ForgotPassword')}>Forgot Password</Text>
      <View style={{marginTop: 33, flexDirection: 'row', alignItems:'center', paddingHorizontal: 72}}>
        <View style={{flex:1, backgroundColor: 'black', height: 1}}></View>
        <Text style={{paddingHorizontal: 15, fontSize: 12, color: '#2E3283'}}>Login with</Text>
        <View style={{flex:1, backgroundColor: 'black', height: 1}}></View>
      </View>
      <View style= {{flexDirection: 'row', marginTop: 28}}>
        <Image source={FacebookLogo} style={{marginRight: 38}}/>
        <Image source={GoogleLogo} />
      </View>
      <View style={{flexDirection: 'row', marginTop: 33}}>
        <Text style={{fontSize: 12, color: '##828282', marginRight: 41}}>App Version</Text>
        <Text style={{fontSize: 12, color: '##828282'}}>2.8.3</Text>
      </View>
      <View style={[styles.footer, styles.shadow]}>
        <Text style={styles.footerText}>Don't have account?</Text>
        <Text style={styles.loginText} onPress={() => navigation.navigate('SignUp')}>Sign up</Text>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  root: {
    flex: 0,
    alignItems: 'center',
    paddingTop: 95,
    backgroundColor: '#F4F4F4',
  },
  inputContainer:{
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'center',
    width: 273,
    height: 43,
    borderRadius: 10,
    paddingHorizontal: 17,
    marginBottom: 17,
  },
  text: {
    marginTop: 17,
    fontSize: 18,
  },
  input: {
    width: 273,
    height: 43,
    borderRadius: 10,
    backgroundColor: 'white',
    paddingLeft: 17,
    marginBottom: 17,
    fontSize: 12,
  },
  button: {
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 12,
  },
  footer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    width: 420,
    height: 55,
    justifyContent: 'center',
  },
  footerText: {
    fontSize: 12,
    color:'#5B5B5B',
  },
  loginText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#5B5B5B',
    marginLeft: 5,
  },
  shadow:{
    shadowColor: 'black',
    shadowOffset: {width: 0, height: -4},
    shadowOpacity: 10,
    shadowRadius: 6,
    elevation: 50,
  },
  logo: {},
});

export default LoginScreen;
