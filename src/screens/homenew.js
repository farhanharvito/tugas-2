import React from 'react';
import {View, Image, Text, Icon, StyleSheet} from 'react-native';
import Banner from '../../assets/MarlinSampleBannerBanner.png';

const HomeScreen = () => {
    return (
        <View style={{flex: 1}}>
            <Image source = {Banner} style={{width: '100%', height: 162}} />
            <View style={{flexDirection: 'row', justifyContent: 'center',}}>
                <Image source={require('../../assets/ic_attraction.png')} style={styles.menuIcon}/>
                <Image source={require('../../assets/ic_ferry_domestic.png')} style={styles.menuIcon} />
                <Image source={require('../../assets/ic_ferry_intl.png')} style={styles.menuIcon} />
                <Image source={require('../../assets/ic_pioneership.png')} style={styles.menuIcon} />
                
            </View>
            <View style={styles.btnMore}>
                <Text style={styles.textMore}>More..</Text>
            </View>
            
        </View>
    );
};

const styles = StyleSheet.create({
    menuIcon: {
        marginHorizontal: 15,
    },
    btnMore:{
        marginTop: 243,
        width:108,
        height: 40,
        backgroundColor: '#2E3283',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    textMore:{
        color: 'white',
        alignSelf: 'center',
        fontSize: 16,
    }
});

export default HomeScreen