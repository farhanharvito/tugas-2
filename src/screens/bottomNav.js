import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './homenew';
import LoginScreen from './login';
import {Image, StyleSheet} from 'react-native'
import React from 'react';
import BookingScreen from './myBooking';
import HelpScreen from './help';
import ProfileScreen from './profile';

const Tab = createBottomTabNavigator();

const BottomNav = () => {
  return (
    <Tab.Navigator screenOptions={{tabBarStyle:{position: 'absolute', height: 64, paddingHorizontal: 24, paddingVertical: 10}}}>
      <Tab.Screen name="Home" component={HomeScreen} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('../../assets/Home.png')}/>
        )
      }} />
      <Tab.Screen name="My Booking" component={BookingScreen} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('../../assets/MyBooking.png')}/>
        )
      }} />
      <Tab.Screen name="Help" component={HelpScreen} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('../../assets/Help.png')}/>
        )
      }} />
      <Tab.Screen name="Profile" component={ProfileScreen} options={{
        tabBarIcon: ({color}) => (
            <Image source={require('../../assets/Profile.png')}/>
        )
      }} />
    </Tab.Navigator>
  );
}

export default BottomNav